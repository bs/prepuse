=========
 prepuse
=========

-----------------------------------------
prepare a mobile device for specific use
-----------------------------------------

:Author: bernd@bschu.de
:Date: 2023-09-18
:Copyright: Bernd Schumacher <bernd@bschu.de> (2023, 2024)
:License: GNU General Public License, version 3
:Version: 0.1
:Manual section: 1
:Manual group: librem5 utils

SYNOPSIS
========
**prepuse** [**-d**]

DESCRIPTION
===========

Configure a mobile device with a small GUI
to use special settings and start needed programms
for a specific usage

Some usages of a mobile device need preparations,
like starting apps or configuring GSettings(1).
With a small GUI this may be tricky and cost much time.

**prepuse** tries to help with simple **PREP-FILES** that
can run actions or turn config settings on and off
and show the current state.

If enough **PREP-FILES** exist, even a non-technical person
should be able to prepare use cases with **USE-FILES**
that select and configure **PREP-FILES** as *on* or *off*
with personal preference.

Because the usability of a mobile device depends on how easy
and fast usage preparations can be done, **prepuse** exists.
If you find yourself, often doing the same preparations for
some use cases manually, you could try to build **PREP-FILES**
and **USE-FILES** to automate this steps and provide it to the
community.


USE-FILES
=========

A **USE-FILE** defines which **PREP-FILES** to apply
and the values to be given.
When **prepuse** starts, a **USE-FILE** has to be
selected.

The following USE-FILES exist today in */etc/prepuse/*:

**no-gps.use**

  Prepare the use of the mobile device without GPS.
  This will stop pure-maps, and allows to dim and stop
  the Mobile Device if not used.

**pure-maps.use**

  Prepare to use pure-maps.
  This should make sure that GPS is working, GPS access is allowed,
  Local Open Streetmap Data is accessible, the
  Display of the Mobile Device will not automatically stop
  and pure-maps is started.

**normal.use**

  Prepare normal use.
  This file most probably has to be changed by every user,
  because normal is very subjective.

**usbhub.use**

  This use case is helpful, if the librem5 is bottom up,
  lying beside the monitor because the usb-hub cable comes from up side.


PREP-FILES
==========

The following **PREP-FILES** exist in */usr/share/prepuse/* today:


**blank-screen.prep**

  Get or set *org.gnome.desktop.session idle-delay*.
  A **USE-FILE** with *blank_screen=<number> will set the idle-deay to
  <number> secs.
  A **USE-FILE** with blank_screen=never will set idle-delay to 0 secs.
  A **USE-FILE** with blank_screen=<number>min, will set the idle-delay with
  the related secs.

**gnss-check-reset.prep**

  Check state of gnss modules and service and restart if not ok.
  Normally this **PREP-FILE** would not be necessarry, but if
  GPS is not running, it may help to restart GPS,
  without reboot of mobile device.

**gnss-test.prep**

  This **PREP-FILE** uses the script *gnss-test* provided by purism.
  If *gnss-test* is not installed, you will get an error,  with
  some hints how to install *gnss-test*.
  A **USE-FILE** with *gnss_test=on" will start *gnss-test*.

  When GPS is not used for some time, it seems *gnss-test* makes
  it faster to get a GPS FIX, if GPS is queried constantly
  by *gnss-test*

  *gnss-test* can be turned off after the GPS FIX is found and
  *pure-maps* is running.
  But if the output does not change and only displays:

  .. code::

     GPS 0.4
     -------------------------
     longitude      0.00000000
     latitude       0.00000000
     altitude (m)   0.00
     accuracy       99.0
     fix            None
     View sats

     Solution sats

     timestamp      None.00
     Fix time       0.000
     TTFF           0.0

  something is wrong with the GPS service and you should
  probably reboot your mobile device.

**idle-dim.prep**

  can set the gsetting *org.gnome.settings-daemon.plugins.power idle-dim*
  to the values *on* or *off*.

**location.prep**

  can set the gsetting *org.gnome.system.location* to the values *on* or
  *off*.

**orientation.prep**

  can set the orientation of the display, to *normal*, *90*, *180*, or *270*.


**osmscout-server.prep**

  can start and stop the OSM Scout Server GPS offline database.

**pure-maps.prep**

  can start and stop *pure-maps*

**screen-lock.prep**

  cat set the gsettings *org.gnome.desktop.screensaver lock-enabled*
  to the values *on* or *off*.

**wlan.prep**

  can start and stop wlan. If it is sure, that wlan is not available,
  it may save power to stop wi-fi radio.


USE-FILE-RULES
==============

**USE-FILES** live in directory */etc/prepuse/* and have the ending *.use*.
**USE-FILE** can be overwritten, or added with **USE-FILES** in
directory *~/.config/prepuse/*

For each **PREP-FILE** */usr/share/prepuse/<x>.prep* used,
they define a shell variable *<x>=<value>*.
Often only the values *on* or *off* are needed.
The **PREP-FILES** will be applied in the order they are defined.

If the **PREP-FILE** has a dash (*-*) in the name, the defined shell
variable has to replace this with an underscore (*_*) to satisfiy the shell
syntax.

PREP-FILE-RULES
===============

**PREP-FILES** live in directory */usr/share/prepuse/* and have the ending *.prep*
**PREP-FILES** can be overwritten, or added with **PREP-FILES** in
directory *~/.local/share/prepuse/*
Each **PREP-FILE** has to define the functions *status*, and *setval* and the
variable *guiargs* with args shown in the GUI separated by *!*.
The first line of a **PREP-FILE** should start with *!/usr/bin/prepcmd* and it
should be executeable to be used from command-line.

To run a **PREP-FILE** from commandline, the script *prepcmd* exists.

prepcmd
=======

**prepcmd** is a commandline tool used by the GUI tool **prepuse**
that uses **PREP-FILES**.


**Usage**

  *prepcmd [-d] <prep> status|<value>*

**status**

  run status function of /usr/share/prepuse/<prep>.prep

**<value>**

  run *setval* function of /usr/share/prepuse/<prep>.prep
  with argument *<value>*

**-d**

  Run **prepcmd** in debug mode.

prepuse
=======

**prepuse** is the GUI programm that used **USE-FILES** and **PREP-FILES**
to prepare the mobile device for a usage scenario.
Normally **prepuse** is started from the GUI with a *.desktop* file
and needs no options.

To see more details from the GUI it is possible to click **detail mode**

In detail mode it is possible to check current PREP states
and list the wanted PREP states for a USE.
Optinal the wanted PREP states can be modified.
After clicking "RUN" the current PREP states can be
checked again.

However the default is **easy** mode. In this mode it is
possible to select the wanted USE and press *run easy*,
without listing and checking PREP states.

If **prepuse** is started from commandline, the following options can be used:

**-d**

  Run **prepuse** in debug mode.

SEE ALSO
========

GSettings(1)
