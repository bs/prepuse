all: prepuse_*.deb

prepuse_*.deb: Makefile src/prepuse src/prepuse-1.rst src/*.use \
  src/prepuse.desktop src/*.prep
	@if [ $(USER) = "root" ]; then echo "Makefile: do not run as root"; exit 1; fi
	@(cd src; debuild --lintian-opts -i -- binary)

clean:
	@(cd src; debuild -- clean)
	@rm -f prepuse_*.buildinfo  prepuse_*.build prepuse_*.changes \
	  prepuse_*.dsc prepuse_*.tar.xz

clobber: clean
	@rm -f prepuse_*.deb

install: /var/lib/dpkg/info/prepuse.list

/var/lib/dpkg/info/prepuse.list: prepuse_*.deb
	@sudo dpkg --force-confmiss -i prepuse_*.deb
